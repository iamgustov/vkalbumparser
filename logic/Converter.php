<?php
/**
 * Class Converter
 * Конвертирует VK-response в наши сущности
 */

class Converter {
    public static function convertToAlbum($response){
        try{
            $album = new Album();

            $album->setId($response['aid']);
            $album->setThumbId($response['thumb_id']);
            $album->setTitle($response['title']);
            $album->setDescription($response['description']);
            $album->setCreatedAt($response['created']);
            $album->setUpdatedAt($response['updated']);
            $album->setSize($response['size']);

            return $album;
        } catch(Exception $ex) {
            throw new $ex;
        }
    }

    public static function convertToAlbums($response){
        try{
            $albums = array();

            foreach ($response as $r){
                $albums[] = self::convertToAlbum($r);
            }

            return $albums;
        } catch (Exception $ex) {
            throw new $ex;
        }
    }

    public static function convertToPhoto($response){
        try{
            $photo = new Photo();

            $photo->setId($response['pid']);
            $photo->setSrc($response['src']);
            $photo->setSrcBig($response['src_xxbig']);
            $photo->setTitle($response['text']);
            $photo->setCreatedAt($response['created']);

            return $photo;
        } catch(Exception $ex) {
            throw new $ex;
        }
    }

    public static function convertToPhotos($response){
        try{
            $photos = array();

            foreach ($response as $r){
                $photos[] = self::convertToPhoto($r);
            }

            return $photos;
        } catch(Exception $ex) {
            throw new $ex;
        }
    }
} 