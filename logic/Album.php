<?php
/**
 * Class Album
 * Определяет сущность альбомов VK
 */

class Album {
    private $id;
    private $thumbId;
    private $title;
    private $description;
    private $createdAt;
    private $updatedAt;
    private $size;
    private $photoList;

    public function __construct($id = -1, $thumbId = -1, $title = 'unknown', $description = 'unknown', $createdAt = -1, $updatedAt = -1, $size = 0, $photoList = array()){
        $this->id = $id;
        $this->thumbId = $thumbId;
        $this->title = $title;
        $this->description = $description;
        $this->createdAt = $createdAt;
        $this->updatedAt;
        $this->size = $size;
        $this->photoList = $photoList;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getThumbId(){
        return $this->thumbId;
    }

    public function setThumbId($thumbId){
        $this->thumbId = $thumbId;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setTitle($title){
        $this->title = $title;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        $this->description = $description;
    }

    public function getCreatedAt(){
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt){
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(){
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt){
        $this->updatedAt = $updatedAt;
    }

    public function getSize(){
        return $this->size;
    }

    public function setSize($size){
        $this->size = $size;
    }

    public function getPhotoList(){
        return $this->photoList;
    }

    public function setPhotoList($photoList){
        $this->photoList = $photoList;
    }
} 