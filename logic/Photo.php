<?php
/**
 * Class Photo
 * Описывает сущность фото VK
 */

class Photo {
    private $id;
    private $src;
    private $srcBig;
    private $title;
    private $createdAt;

    public function __construct($id = -1, $src = 'unknown', $srcBig = 'unknown', $title = 'unknown', $createdAt = -1){
        $this->id = $id;
        $this->src = $src;
        $this->srcBig = $srcBig;
        $this->title = $title;
        $this->createdAt = $createdAt;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getSrc(){
        return $this->src;
    }

    public function setSrc($src){
       $this->src = $src;
    }

    public function getSrcBig(){
        return $this->srcBig;
    }

    public function setSrcBig($srcBig){
        $this->srcBig = $srcBig;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setTitle($title){
        $this->title = $title;
    }

    public function getCreatedAt(){
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt){
        $this->createdAt = $createdAt;
    }
} 