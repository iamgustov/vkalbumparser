<?php
    header('Content-Type: text/html; charset=utf-8');
    define('GROUP_ID', '13846031');
    define('APP_ID', '4169815');
    define('APP_SECRET', 'MvVs11Hza23B7ppqXVWL');

    require_once('./libs/VK.php');
    require_once('./logic/Album.php');
    require_once('./logic/Photo.php');
    require_once('./logic/Converter.php');
    require_once('./logic/Helper.php');
    require_once('./tests/Testify.php');

    $tf = new \Testify\Testify('VK Test Suite');

    $tf->beforeEach(function($tf){
        $tf->data->vk = new \VK\VK(APP_ID, APP_SECRET);
    });

    // Тест на проверку получения всех альбомов
    $tf->test('Test of retrieving albums', function($tf){
        $vk = $tf->data->vk;

        $albumsResponse = $vk->api('photos.getAlbums', array(
            'gid' => GROUP_ID
        ));

        $albums = $albumsResponse['response'];

        $albums = Converter::convertToAlbums($albums);

        $tf->assertEqual(count($albums), 13);
    });

    // Тест на проверку получения всех фоток в альбоме
    $tf->test('Test of retrieving photos', function($tf){
        $vk = $tf->data->vk;

        $albumsResponse = $vk->api('photos.getAlbums', array(
            'gid' => GROUP_ID
        ));

        $albums = $albumsResponse['response'];

        $albums = Converter::convertToAlbums($albums);

        $album = $albums[0];

        $photosResponse = $vk->api('photos.get', array(
            'gid' => GROUP_ID,
            'aid' => $album->getId()
        ));

        $photos = $photosResponse['response'];

        $photos = Converter::convertToPhotos($photos);

        $tf->assertEqual(count($photos), 10);
    });

    $tf->run();
?>