<?php
    header('Content-Type: text/html; charset=utf-8');
    define('GROUP_ID', '13846031');
    define('APP_ID', '4169815');
    define('APP_SECRET', 'MvVs11Hza23B7ppqXVWL');

    require_once('./libs/VK.php');
    require_once('./logic/Album.php');
    require_once('./logic/Photo.php');
    require_once('./logic/Converter.php');
    require_once('./logic/Helper.php');

    $vk = new \VK\VK(APP_ID, APP_SECRET);

    $model = array();

    $albumsResponse = $vk->api('photos.getAlbums', array(
        'gid' => GROUP_ID
    ));

    $albums = $albumsResponse['response'];

    foreach ($albums as $a){
        $album = Converter::convertToAlbum($a);

        $photosResponse = $vk->api('photos.get', array(
            'gid' => GROUP_ID,
            'aid' => $album->getId()
        ));

        $photos = $photosResponse['response'];

        $album->setPhotoList(Converter::convertToPhotos($photos));

        $model[] = $album;
    }


//    Helper::printDebug($model);
//    Helper::printDebug(Converter::convertToAlbums($albums));

    foreach ($model as $item){
        print '<h3>' . $item->getTitle() . '</h3>';
        print '<h6>' . $item->getDescription() . '<br/>Фотографий в альбоме: ' . $item->getSize() . '</h6>';

        foreach ($item->getPhotoList() as $photo){
            print Helper::getImageLink($photo->getSrc()) . '<br/><br/>' . $photo->getTitle() . '<br/><br/>';
        }

        print '<br/><br/>';
    }
?>